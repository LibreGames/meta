# Libre Games organization

This repository holds various shared files relevant to the Libre Games
organization as a whole (logo, scripts, guidelines, etc.). See the
[Libre Games website](https://libregames.gitlab.io) for more details
about the organization.

Its [issue tracker](https://gitlab.com/LibreGames/meta/issues) is also
used for organization-related topics, as well as its
[wiki](https://gitlab.com/LibreGames/meta/wikis).
